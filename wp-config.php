<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'wp_all_theme_demo' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', 'root' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'cTPIxz9Ud4n+Q8 !a+j*nFKD?+W.CINmx].5l!d%lV~NCf}%&<s1*|@t,3zAELDW' );
define( 'SECURE_AUTH_KEY',  '^gS%Pjt*R3z/RtWI~_B>?]U.}[e8ux5&nX&(,}EF/b)5m/ hA+/}sxhvz^a+p9.[' );
define( 'LOGGED_IN_KEY',    'G<@qh]f7| qS!mQc-IDO&,xSs.E)Yn0y;6v3a>.`.$[;86Q1^55D%b>4mP{Idt]e' );
define( 'NONCE_KEY',        'rZWPB)$y}O[sG3X[b4;pWNVFzlO}:y:>}C>@?,V9o|QTk_DQ4Od%eE2SP=T.o$rx' );
define( 'AUTH_SALT',        '?f0${`uyhzD1wayrS*B}hi5$l,Xr`!%WePWL/6r1RJk3InKbxDk}!ItAraG/2&1d' );
define( 'SECURE_AUTH_SALT', '~0Cn *,)j*2D]#aom~NY2G%LbEMS,q~0Y<+y- [7_8uTEB>Nd8ZHD9Gb3u&}<8Mn' );
define( 'LOGGED_IN_SALT',   'u!ltP$k_.gGo+DwfGn<Pj&Tu=R!z!n{KZ.{FXnQgR$ZRGsMyk^98E=E8A)Iq0P)p' );
define( 'NONCE_SALT',       'GCG$2fS;!2oMa+A~EE0DFfrxsC];[T0PeFDbgW~1&{!$3$=W^7]qg2`pZJRR4.|)' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
