<div class="wrap">
    <h1>Order</h1>
    <table class="wp-list-table widefat fixed striped table-view-list posts">
            <thead>
                <tr>
                    <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1">Select All</label><input id="cb-select-all-1" type="checkbox"></td>
                    <th scope="col" id="order_number" class="manage-column column-order_number column-primary sortable desc"><a href=""><span>Order</span><span class="sorting-indicator"></span></a></th>
                    <th scope="col" id="order_date" class="manage-column column-order_date sorted asc"><a href=""><span>Detail</span><span class="sorting-indicator"></span></a></th>
                    <th scope="col" id="order_status" class="manage-column column-order_status">Status</th>
                    <th scope="col" id="billing_address" class="manage-column column-billing_address hidden">Billing</th>
                    <th scope="col" id="shipping_address" class="manage-column column-shipping_address hidden">Ship to</th>
                    <th scope="col" id="order_total" class="manage-column column-order_total sortable desc"><a href=""><span>Total</span><span class="sorting-indicator"></span></a></th>
                    <th scope="col" id="wc_actions" class="manage-column column-wc_actions hidden">Actions</th>
                </tr>
            </thead>
            <tbody id="the-list">

            <?php
            
            
            $author_id = get_current_user_id();
            // $author_id = 2;
            
            global $wpdb; 

            $sql_product_id = "select id from " . $wpdb->prefix . "posts  where post_type = 'product' and
            post_author = " . $author_id . " and
            post_status = 'publish'";

            $product_id_array = $wpdb->get_results($sql_product_id);

            


            $sql = "select a.order_id
                    from " . $wpdb->prefix . "wc_order_product_lookup as a 
                    where a.order_id in 
                    (select b.ID from " . $wpdb->prefix . "posts as b where b.post_type = 'shop_order') 
                    and a.product_id in 
                    (select c.id from " . $wpdb->prefix . "posts as c where c.post_type = 'product' and
                    c.post_author = " . $author_id . " and
                    c.post_status = 'publish') group by a.order_id";
            $order_list = $wpdb->get_results( $sql ); //trả về dữ liệu trong biến $data 
            // var_dump($order_list);
            
            foreach ($order_list as $order_id):
                /**  Get an instance of the WC_Order object */
                $order = wc_get_order( $order_id->order_id );

                $order_data = $order->get_data(); // The Order data

                $order_id = $order_data['id'];
                $order_parent_id = $order_data['parent_id'];
                $order_status = $order_data['status'];
                $order_currency = $order_data['currency'];
                $order_version = $order_data['version'];
                $order_payment_method = $order_data['payment_method'];
                $order_payment_method_title = $order_data['payment_method_title'];
                $order_payment_method = $order_data['payment_method'];

                ## Creation and modified WC_DateTime Object date string ##

                // Using a formated date ( with php date() function as method)
                $order_date_created = $order_data['date_created']->date('Y-m-d H:i:s');
                $order_date_modified = $order_data['date_modified']->date('Y-m-d H:i:s');


                // Using a timestamp ( with php getTimestamp() function as method)
                $order_timestamp_created = $order_data['date_created']->getTimestamp();
                $order_timestamp_modified = $order_data['date_modified']->getTimestamp();



                $order_discount_total = $order_data['discount_total'];
                $order_discount_tax = $order_data['discount_tax'];
                $order_shipping_total = $order_data['shipping_total'];
                $order_shipping_tax = $order_data['shipping_tax'];
                $order_total = $order_data['total'];
                $order_total_tax = $order_data['total_tax'];
                $order_customer_id = $order_data['customer_id']; // ... and so on




                ## BILLING INFORMATION:

                $order_billing_first_name = $order_data['billing']['first_name'];
                $order_billing_last_name = $order_data['billing']['last_name'];
                $order_billing_company = $order_data['billing']['company'];
                $order_billing_address_1 = $order_data['billing']['address_1'];
                $order_billing_address_2 = $order_data['billing']['address_2'];
                $order_billing_city = $order_data['billing']['city'];
                $order_billing_state = $order_data['billing']['state'];
                $order_billing_postcode = $order_data['billing']['postcode'];
                $order_billing_country = $order_data['billing']['country'];
                $order_billing_email = $order_data['billing']['email'];
                $order_billing_phone = $order_data['billing']['phone'];



                ## SHIPPING INFORMATION:

                $order_shipping_first_name = $order_data['shipping']['first_name'];
                $order_shipping_last_name = $order_data['shipping']['last_name'];
                $order_shipping_company = $order_data['shipping']['company'];
                $order_shipping_address_1 = $order_data['shipping']['address_1'];
                $order_shipping_address_2 = $order_data['shipping']['address_2'];
                $order_shipping_city = $order_data['shipping']['city'];
                $order_shipping_state = $order_data['shipping']['state'];
                $order_shipping_postcode = $order_data['shipping']['postcode'];
                $order_shipping_country = $order_data['shipping']['country'];


                ?>
                    <tr id="post-978" class="author-self level-0 post-978 type-shop_order status-wc-processing post-password-required hentry">
                        <th scope="row" class="check-column">
                            <label class="screen-reader-text" for="cb-select-978">
                            Select Order – September 11, 2019 @ 01:16 PM			</label>
                            <input id="cb-select-978" type="checkbox" name="post[]" value="978">
                            <div class="locked-indicator">
                            <span class="locked-indicator-icon" aria-hidden="true"></span>
                            <span class="screen-reader-text">
                            “Order – September 11, 2019 @ 01:16 PM” is locked				</span>
                            </div></th>
                        <td class="order_number column-order_number has-row-actions column-primary" data-colname="Order"><a href="#" class="order-preview" data-order-id="978" title=""></a><a href="http://sitedemoall.local/wp-admin/post.php?post=978&amp;action=edit" class="order-view"><strong>#<?php echo $order_id . " " . $order_billing_first_name . " " . $order_billing_last_name; ?></strong></a></td>
                        <td class="order_date column-order_date" data-colname="Detail">
                            
                                <div class="wc-backbone-modal-content" tabindex="0">
                                        <div class="wc-order-preview-addresses">
                                            <div class="wc-order-preview-address">
                                            <h2>Billing details</h2>
                                            <?php echo $order_shipping_first_name . " - " . $order_shipping_last_name;?><br><?php echo $order_billing_address_1 ;?><br><?php echo $order_billing_city ;?>
                                            <strong>Email</strong>
                                            <a href="mailto:hthanh11it@gmail.com"><?php echo $order_billing_email ;?></a>
                                            <strong>Phone</strong>
                                            <a href="tel:0987654321"><?php echo $order_billing_phone ;?></a>
                                            <strong>Payment via</strong>
                                            <?php echo $order_payment_method ;?>
                                            </div>
                                            <div class="wc-order-preview-address">
                                            <h2>Shipping details</h2>
                                            <strong>Shipping method</strong>
                                            <?php echo $order_shipping_tax;?>
                                            </div>
                                        </div>
                                        <div class="wc-order-preview-table-wrapper">
                                            <table cellspacing="0" class="wc-order-preview-table">
                                            <thead>
                                                <tr>
                                                    <th class="wc-order-preview-table__column--product">Product</th>
                                                    <th class="wc-order-preview-table__column--quantity">Quantity</th>
                                                    <th class="wc-order-preview-table__column--tax">Tax</th>
                                                    <th class="wc-order-preview-table__column--total">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    /**  Get an instance of the WC_Order object */
                                                    // Iterating through each WC_Order_Item_Product objects
                                                    foreach ($order->get_items() as $item_key => $item ):
                                                        
                                                        ## Using WC_Order_Item methods ##
                                                        
                                                        // Item ID is directly accessible from the $item_key in the foreach loop or
                                                        $item_id = $item->get_id();
                                                    
                                                        ## Using WC_Order_Item_Product methods ##
                                                    
                                                        $product      = $item->get_product(); // Get the WC_Product object
                                                        
                                                        
                                                        $product_id   = $item->get_product_id(); // the Product id
                                                    
                                                    
                                                    $item_name    = $item->get_name(); // Name of the product
                                                        $quantity     = $item->get_quantity();  
                                                        $tax_class    = $item->get_tax_class();
                                                        $line_subtotal     = $item->get_subtotal(); // Line subtotal (non discounted)
                                                        $line_subtotal_tax = $item->get_subtotal_tax(); // Line subtotal tax (non discounted)
                                                        $line_total        = $item->get_total(); // Line total (discounted)
                                                        $line_total_tax    = $item->get_total_tax(); // Line total tax (discounted)
                                                    
                                                        $item_type    = $item->get_type(); // Type of the order item ("line_item")
                                                    
                                                        $item_data    = $item->get_data();
                                                    
                                                    $product_name = $item_data['name'];
                                                        $product_id   = $item_data['product_id'];
                                                        $variation_id = $item_data['variation_id'];
                                                        $quantity     = $item_data['quantity'];
                                                        $tax_class    = $item_data['tax_class'];
                                                        $line_subtotal     = $item_data['subtotal'];
                                                        $line_subtotal_tax = $item_data['subtotal_tax'];
                                                        $line_total        = $item_data['total'];
                                                        $line_total_tax    = $item_data['total_tax'];
                                                    
                                                    $product        = $item->get_product(); // Get the WC_Product object
                                                    
                                                        $product_type   = $product->get_type();
                                                        $product_sku    = $product->get_sku();
                                                        $product_price  = $product->get_price();
                                                        $stock_quantity = $product->get_stock_quantity();
                                                    
                                                    
                                                    
                                                        // Next Product False
                                                        $check = false;
                                                        foreach($product_id_array as $item):
                                                            if ($item->id == $product_id) {
                                                                $check = true;
                                                                // echo  $product_id ."--------" . $item->id . "<br>";
                                                                break;
                                                            }
                                                        endforeach;
                                                        
                                                        // Next Product False
                                                        
                                                        if(!$check):
                                                            continue; 
                                                        endif;   
                                                    
                                                        ?>
                                                <tr class="wc-order-preview-table__item wc-order-preview-table__item--20">
                                                    <td class="wc-order-preview-table__column--product">
                                                        <?php echo $product_name;?>
                                                        <div class="wc-order-item-sku"><?php echo $product_sku;?></div>
                                                        <table cellspacing="0" class="wc-order-item-meta"></table>
                                                    </td>
                                                    <td class="wc-order-preview-table__column--quantity"><?php echo $quantity;?></td>
                                                    <td class="wc-order-preview-table__column--tax"><span class="woocommerce-Price-amount amount"><bdi><?php echo $line_total_tax;?><span class="woocommerce-Price-currencySymbol">₫</span></bdi></span></td>
                                                    <td class="wc-order-preview-table__column--total"><span class="woocommerce-Price-amount amount"><bdi><?php echo $line_total;?><span class="woocommerce-Price-currencySymbol">₫</span></bdi></span></td>
                                                </tr>
                                                <?php
                                                    endforeach;
                                                    ?>
                                            </tbody>
                                            </table>
                                        </div>
                                </div>
                        </td>
                        <td class="order_status column-order_status" data-colname="Status"><mark class="order-status status-processing tips"><span><?php echo $order_status; ?></span></mark></td>
                        <td class="billing_address column-billing_address hidden" data-colname="Billing"><>?php echo $order_shipping_address_1 ;?<span class="description">via Cash on delivery</span></td>
                        <td class="shipping_address column-shipping_address hidden" data-colname="Ship to"><a target="_blank" href="">Prabin Jha, Carey and Norton LLC, Kathmandu, Sint do et sint et q, Kathmandu, Bagmati, 44600, Nepal</a></td>
                        <td class="order_total column-order_total" data-colname="Total"><span class="tips"><span class="woocommerce-Price-amount amount"><?php echo  $order_total;?><span class="woocommerce-Price-currencySymbol">đ</span></span></span></td>
                        <td class="wc_actions column-wc_actions hidden" data-colname="Actions">
                            <p><a class="button wc-action-button wc-action-button-complete complete" href=" aria-label="Complete">Complete</a></p></td>
                    </tr>
                        
        <?php
        endforeach;
        ?>     

        </tbody>
        <tfoot>
            <tr>
                <td class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-2">Select All</label><input id="cb-select-all-2" type="checkbox"></td>
                <th scope="col" class="manage-column column-order_number column-primary sortable desc"><a href=""><span>Order</span><span class="sorting-indicator"></span></a></th>
                <th scope="col" class="manage-column column-order_date sorted asc"><a href=""><span>Detail</span><span class="sorting-indicator"></span></a></th>
                <th scope="col" class="manage-column column-order_status">Status</th>
                <th scope="col" class="manage-column column-billing_address hidden">Billing</th>
                <th scope="col" class="manage-column column-shipping_address hidden">Ship to</th>
                <th scope="col" class="manage-column column-order_total sortable desc"><a href=""><span>Total</span><span class="sorting-indicator"></span></a></th>
                <th scope="col" class="manage-column column-wc_actions hidden">Actions</th>
            </tr>
        </tfoot>
    </table>       
</div>
