                            <div class="wc-backbone-modal-content" tabindex="0">
                                <section class="wc-backbone-modal-main" role="main">
                                    <header class="wc-backbone-modal-header">
                                        <mark class="order-status status-completed"><span>Completed</span></mark>
                                        <h1>Order #1020</h1>
                                        <button class="modal-close modal-close-link dashicons dashicons-no-alt">
                                        <span class="screen-reader-text">Close modal panel</span>
                                        </button>
                                    </header>
                                    <article style="max-height: 344.25px;">
                                        <div class="wc-order-preview-addresses">
                                            <div class="wc-order-preview-address">
                                            <h2>Billing details</h2>
                                            Dung Nguyen<br>Dai Lanh<br>Quang Nam
                                            <strong>Email</strong>
                                            <a href="mailto:hthanh11it@gmail.com">hthanh11it@gmail.com</a>
                                            <strong>Phone</strong>
                                            <a href="tel:0987654321">0987654321</a>
                                            <strong>Payment via</strong>
                                            Cash on delivery
                                            </div>
                                            <div class="wc-order-preview-address">
                                            <h2>Shipping details</h2>
                                            <a href="https://maps.google.com/maps?&amp;q=Dai%20Lanh%2C%20%2C%20Quang%20Nam%2C%20%2C%20440000%2C%20VN&amp;z=16" target="_blank">Dung Nguyen<br>Dai Lanh<br>Quang Nam</a>
                                            <strong>Shipping method</strong>
                                            Free shipping
                                            </div>
                                        </div>
                                        <div class="wc-order-preview-table-wrapper">
                                            <table cellspacing="0" class="wc-order-preview-table">
                                                <thead>
                                                    <tr>
                                                        <th class="wc-order-preview-table__column--product">Product</th>
                                                        <th class="wc-order-preview-table__column--quantity">Quantity</th>
                                                        <th class="wc-order-preview-table__column--tax">Tax</th>
                                                        <th class="wc-order-preview-table__column--total">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                        <?php
                        

                        /**  Get an instance of the WC_Order object */
                        // Iterating through each WC_Order_Item_Product objects
                        foreach ($order->get_items() as $item_key => $item ):
                            
                            ## Using WC_Order_Item methods ##
                            
                            // Item ID is directly accessible from the $item_key in the foreach loop or
                            $item_id = $item->get_id();

                            ## Using WC_Order_Item_Product methods ##

                            $product      = $item->get_product(); // Get the WC_Product object
                            
                            
                            $product_id   = $item->get_product_id(); // the Product id
	
	
							$item_name    = $item->get_name(); // Name of the product
                            $quantity     = $item->get_quantity();  
                            $tax_class    = $item->get_tax_class();
                            $line_subtotal     = $item->get_subtotal(); // Line subtotal (non discounted)
                            $line_subtotal_tax = $item->get_subtotal_tax(); // Line subtotal tax (non discounted)
                            $line_total        = $item->get_total(); // Line total (discounted)
                            $line_total_tax    = $item->get_total_tax(); // Line total tax (discounted)
	
                            $item_type    = $item->get_type(); // Type of the order item ("line_item")
	
                            $item_data    = $item->get_data();

							$product_name = $item_data['name'];
                            $product_id   = $item_data['product_id'];
                            $variation_id = $item_data['variation_id'];
                            $quantity     = $item_data['quantity'];
                            $tax_class    = $item_data['tax_class'];
                            $line_subtotal     = $item_data['subtotal'];
                            $line_subtotal_tax = $item_data['subtotal_tax'];
                            $line_total        = $item_data['total'];
                            $line_total_tax    = $item_data['total_tax'];
	
							$product        = $item->get_product(); // Get the WC_Product object

                            $product_type   = $product->get_type();
                            $product_sku    = $product->get_sku();
                            $product_price  = $product->get_price();
                            $stock_quantity = $product->get_stock_quantity();
	
	
	
                            // Next Product False
                            $check = false;
                            foreach($product_id_array as $item):
                                if ($item->id == $product_id) {
                                    $check = true;
                                    // echo  $product_id ."--------" . $item->id . "<br>";
                                    break;
                                }
                            endforeach;
                            
                            // Next Product False
                            
                            if(!$check):
                                continue; 
                            endif;   

                            ?>

                                                <tr class="wc-order-preview-table__item wc-order-preview-table__item--20">
                                                    <td class="wc-order-preview-table__column--product">
                                                        <?php echo $product_name;?>
                                                        <div class="wc-order-item-sku"><?php echo $quantity;?></div>
                                                        <table cellspacing="0" class="wc-order-item-meta"></table>
                                                    </td>
                                                    <td class="wc-order-preview-table__column--quantity"><?php echo $quantity;?></td>
                                                    <td class="wc-order-preview-table__column--tax"><span class="woocommerce-Price-amount amount"><bdi><?php echo $line_total_tax;?><span class="woocommerce-Price-currencySymbol">₫</span></bdi></span></td>
                                                    <td class="wc-order-preview-table__column--total"><span class="woocommerce-Price-amount amount"><bdi><?php echo $line_total;?><span class="woocommerce-Price-currencySymbol">₫</span></bdi></span></td>
                                                </tr>

                        <?php
                        endforeach;
                        ?>

                                </tbody>
                                </table>
                            </div>
                        </article>
                        <footer>
                            <div class="inner">
                                <a class="button button-primary button-large" aria-label="Edit this order" href="http://sitedemoall.local/wp-admin/post.php?action=edit&amp;post=1020">Edit</a>
                            </div>
                        </footer>
                    </section>
                </div>

                <?php   
                endforeach;
                ?>